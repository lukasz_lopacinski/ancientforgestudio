﻿namespace AFSInterview
{
    using System.Collections.Generic;
    using UnityEngine;
    using System.Collections;

    public class SimpleTowerType2 : MonoBehaviour
    {
        [SerializeField] private GameObject bulletPrefab;
        [SerializeField] private Transform bulletSpawnPoint;
        [SerializeField] private float firingRate;
        [SerializeField] private float firingRange;




        private float fireTimer;
        private Enemy targetEnemy;

        private IReadOnlyList<Enemy> enemies;

        public void Initialize(IReadOnlyList<Enemy> enemies)
        {
            this.enemies = enemies;
            fireTimer = firingRate;
        }

        private void Start()
        {
            StartCoroutine(BurstShoot());
        }

        private void Update()
        {
            //Instantiate(bulletPrefab, transform.position, Quaternion.identity).GetComponent<Bullet>();
            targetEnemy = FindClosestEnemy();


            if (targetEnemy != null)
            {
                var lookRotation = Quaternion.LookRotation(targetEnemy.transform.position - transform.position);
                transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, lookRotation.eulerAngles.y, transform.rotation.eulerAngles.z);
            }
            }

        private Enemy FindClosestEnemy()
        {
            Enemy closestEnemy = null;
            var closestDistance = float.MaxValue;

            foreach (var enemy in enemies)
            {
                var distance = (enemy.transform.position - transform.position).magnitude;
                if (distance <= firingRange && distance <= closestDistance)
                {
                    closestEnemy = enemy;
                    closestDistance = distance;
                }

            }

            return closestEnemy;
        }

        // Spawning three bullets in a row in burst shot type and disactivate tower for 5s
       IEnumerator BurstShoot()
        {
            while (true)
            {
                if (targetEnemy != null)
                {
                    for (int i = 0; i < 3; i++)
                    {
                        var bullet = Instantiate(bulletPrefab, bulletSpawnPoint.position, Quaternion.identity).GetComponent<Bullet>();
                        bullet.Initialize(targetEnemy.gameObject);

                    }
                    yield return new WaitForSeconds(0.25f);
                }
                yield return new WaitForSeconds(5.0f);
            }
           
           
        }

       }
    }



