﻿namespace AFSInterview
{
    using UnityEngine;

    public class Bullet : MonoBehaviour
    {
        [SerializeField] private float speed;

        private GameObject targetObject;

        public void Initialize(GameObject target)
        {
            targetObject = target;
        }

        private void Update()
        {

            //Checking if the bullet have a target to follow. If not destroying it
            if (targetObject == null)
            {
                Destroy(gameObject);
                Debug.Log("Enemy not there");
            }

            //Checking if there is a GameObject to reffere to
            if (targetObject)
            {
            var direction = (targetObject.transform.position - transform.position).normalized;

            transform.position += direction * speed * Time.deltaTime;
            }


            //Destroying objects based on triger insted of destroying objects based on distance calculation.
        }        
        private void OnTriggerEnter(Collider other)
        {
            Destroy(other.gameObject);
            Destroy(gameObject);
        }



    }
}  